# Duo DAG Updater

This script updates the DAG version running on docker. 
Run this script from the DAG servers.

Usage: ./duo_dag_updater.sh

Confluence: https://hebecom.atlassian.net/wiki/spaces/SEC/pages/735742313/DRAFT+-+Duo+Access+Gateway+Documentation#Updates%2FPatching

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
