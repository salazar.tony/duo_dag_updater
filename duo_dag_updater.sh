#!/bin/sh

# Author: Tony Salazar
# Date:13APR2021
# Description: This script will remove all running Duo DAG docker images, download the latest from Duo and run it with docker-compose


#set -e  #Exit immediately if a command exits with a non-zero status.

ARCHIVE=/etc/docker/archive
DOCKERHOME=/etc/docker
LOGFILE=update.log
CURRENTVER=`ls /etc/docker/*.yml | cut -d"/" -f 4`

# # Run as root check
# if [ "$EUID" -ne 0 ]
#   then echo "Please run as root"
#   exit
# fi

# Download the latest version and get version number
sudo wget --quiet --content-disposition https://dl.duosecurity.com/access-gateway-latest.yml | tee -a $LOGFILE
LATESTVER=`ls access-gateway-*.yml`

echo "Current DAG version: $CURRENTVER"
echo "Latest DAG version: $LATESTVER"
echo 
read -p "Press [ENTER] to begin Duo DAG update to latest version."
echo "Backing up data/ and .yml files" | tee -a $LOGFILE

if [ ! -d "$ARCHIVE" ]; then
    sudo mkdir -p $ARCHIVE 
fi

sudo mv $DOCKERHOME/access-gateway-*.yml $ARCHIVE
ls -lah $ARCHIVE

# Create backup of data folder in pwd
docker cp access-gateway:/data data

# Restore data 
#docker cp data/. access-gateway:/data
# Set file permissions
#docker exec -ti --user=0 access-gateway chown -R www-data:www-data /data

# Move latest to DOCKERHOME
sudo mv access-gateway*.yml $DOCKERHOME

# Edit permissions
sudo chmod 644 $DOCKERHOME/access-gateway-*.yml
ls -lah $DOCKERHOME/access-gateway-*.yml


echo  "Removing all docker images." | tee -a $LOGFILE
docker container stop $(docker container ls -aq); docker system prune -f | tee -a $LOGFILE

echo "Starting new image in daemon mode" | tee -a $LOGFILE
/usr/local/bin/docker-compose -p access-gateway -f $(ls /etc/docker/access-gateway-*.yml) up -d | tee -a $LOGFILE 2>&1

sleep 5
docker ps
echo "Finished Duo DAG update, see $LOGFILE.  `date`" | tee -a $LOGFILE
